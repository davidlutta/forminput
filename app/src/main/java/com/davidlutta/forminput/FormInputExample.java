package com.davidlutta.forminput;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class FormInputExample extends AppCompatActivity {
    private EditText mStudentNameEditText;
    private EditText mStudentEmailEditText;
    private CheckBox mCheckbox;
    private DatePicker mStudentDOB;
    private Calendar calendar;
    private Spinner mSpinner;
    private String[] status = {"full-time", "part-time", "student", "other"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_input);
        mStudentNameEditText = (EditText) findViewById(R.id.studentNameEditText);
        mStudentEmailEditText = (EditText) findViewById(R.id.studentEmailEditText);
        mStudentDOB = (DatePicker) findViewById(R.id.dateOfBirthDatePicker);
        mSpinner = (Spinner) findViewById(R.id.spinner);
        calendar = Calendar.getInstance();
        mCheckbox = (CheckBox) findViewById(R.id.terms_and_conditions_checkBox);
        mSpinner.setAdapter(new ArrayAdapter<String>(this, R.layout.spinner_list_item, status));
    }


    private void getDetails() {
        String name = mStudentNameEditText.getText().toString().trim();
        String email = mStudentEmailEditText.getText().toString().trim();
        boolean checkBoxAns = mCheckbox.isChecked();
        if (name.equals("")) {
            mStudentNameEditText.setError("Please Enter Your Name");
        }

        if (email.equals("")) {
            mStudentEmailEditText.setError("Please Enter Your Email");
        }

        String date = getDate();
        int selectedItem = mSpinner.getSelectedItemPosition();
        String selectedStatus = status[selectedItem];
        if (name.length() > 0 && email.length() > 0) {
            if (!mCheckbox.isChecked()) {
                popupToast("Sorry, you must agree the terms even though you don't know what they are!!");
                return;
            }
            new AlertDialog.Builder(this).setTitle("Details Entered").setMessage(
                    String.format("Details Entered: \nName: %s\nDOB: %s\nEmail: %s\nStatus: %s", name, date, email, selectedStatus)
            ).setNeutralButton("Back", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                }
            }).show();
        }
    }

    private String getDate() {
        int day = mStudentDOB.getDayOfMonth();
        int month = mStudentDOB.getMonth();
        int year = mStudentDOB.getYear();
        calendar.set(year, month, day);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        calendar.set(year, month, day);
        return sdf.format(calendar.getTime());
    }

    public void submitInfo(View view) {
        getDetails();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_exit:
                popupToast("You want to exit but why not just start using another application?");
                break;
            case R.id.action_next:
                getDetails();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    private void popupToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
